package yellowfish.convert;

import java.util.ArrayList;
import java.util.List;

public class People
{
    private ArrayList<Person> persons;

    public People()
    {
        this.persons = new ArrayList<Person>();
    }

    public Person addPerson(String firstName, String lastName)
    {
        Person person = new Person(firstName, lastName);
        this.persons.add(person);

        return person;
    }

    public List<Person> getPersons()
    {
        return this.persons;
    }
}
