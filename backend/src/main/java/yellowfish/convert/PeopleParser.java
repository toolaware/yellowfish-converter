package yellowfish.convert;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Parses a text file and creates the corresponding structure of a People, Persons and FamilyMembers.
 */
public class PeopleParser
{
    private BufferedReader reader = null;
    private String line = null;
    private String[] parts = null;

    private void initBuffers(InputStream inputStream)
    {
        reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    private void cleanup()
    {
        reader = null;
        parts = null;
        line = null;
    }

    public People parse(InputStream inputStream) throws Exception
    {
        initBuffers(inputStream);

        try
        {
            People people = new People();

            if (readLine())
                doParse(people);

            return people;
        }
        catch (Exception e)
        {
            reader.close();
            throw e;
        }
        finally
        {
            cleanup();
        }
    }

    private void doParse(People people) throws Exception
    {
        while (true)
        {
            if (isPersonDefinition())
            {
                Person person = people.addPerson(parts[1], parts[2]);

                if (!readLine())
                    return;

                while (true)
                {
                    if (isPhoneNumberDefinition())
                    {
                        person.setMobilePhone(parts[1]);
                        person.setHomePhone(parts[2]);

                        if (!readLine())
                            return;
                    }
                    else if (isAddressDefinition())
                    {
                        person.setStreet(parts[1]);
                        person.setCity(parts[2]);
                        person.setPostalCode(parts.length > 3 ? parts[3] : null);

                        if (!readLine())
                            return;
                    }
                    else if (isFamilyMemberDefinition())
                    {
                        if (!readFamilyMember(person))
                            return;
                    }
                    else
                        break;
                }
            }
        }
    }

    private boolean readFamilyMember(Person person) throws Exception
    {
        FamilyMember familyMember = person.addFamilyMember(parts[1], parts[2]);

        if (!readLine())
            return false;

        while (true)
        {
            if (isPhoneNumberDefinition())
            {
                familyMember.setMobilePhone(parts[1]);
                familyMember.setHomePhone(parts[2]);

                if (!readLine())
                    return false;
            }
            else if (isAddressDefinition())
            {
                familyMember.setStreet(parts[1]);
                familyMember.setCity(parts[2]);
                familyMember.setPostalCode(parts.length > 3 ? parts[3] : null);

                if (!readLine())
                    return false;
            }
            else // Unknown to us, let parent handle it
                break;
        }

        return true;
    }

    private boolean readLine() throws Exception
    {
        this.line = this.reader.readLine();
        if (this.line == null)
            return false;

        this.parts = this.line.split("\\|");
        return true;
    }

    private boolean isPersonDefinition()
    {
        return parts[0].equals("P");
    }

    private boolean isPhoneNumberDefinition()
    {
        return parts[0].equals("T");
    }

    private boolean isAddressDefinition()
    {
        return parts[0].equals("A");
    }

    private boolean isFamilyMemberDefinition()
    {
        return parts[0].equals("F");
    }
}
