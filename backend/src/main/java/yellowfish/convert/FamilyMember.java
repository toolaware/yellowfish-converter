package yellowfish.convert;

/**
 * Defines a family member to a Person.
 */
public class FamilyMember
{
    private String name;
    private String born;
    private String mobilePhone;
    private String homePhone;
    private String street;
    private String city;
    private String postalCode;

    public FamilyMember()
    {
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setBorn(String born)
    {
        this.born = born;
    }

    public String getName()
    {
        return name;
    }

    public String getBorn()
    {
        return born;
    }

    public String getMobilePhone()
    {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone)
    {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone()
    {
        return homePhone;
    }

    public void setHomePhone(String homePhone)
    {
        this.homePhone = homePhone;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public boolean hasAddress()
    {
        return this.city != null || this.street != null || this.postalCode != null;
    }

    public boolean hasPhoneNumbers()
    {
        return this.mobilePhone != null || this.homePhone != null;
    }
}
