package yellowfish.convert;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a person that is included in a Peoplel.
 */
public class Person
{
    private String firstName;
    private String lastName;
    private String mobilePhone;
    private String homePhone;
    private String street;
    private String city;
    private String postalCode;

    private ArrayList<FamilyMember> familyMembers;

    public Person(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.familyMembers = new ArrayList<FamilyMember>();
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getMobilePhone()
    {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone)
    {
        this.mobilePhone = mobilePhone;
    }

    public String getHomePhone()
    {
        return homePhone;
    }

    public void setHomePhone(String homePhone)
    {
        this.homePhone = homePhone;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public FamilyMember addFamilyMember(String name, String born)
    {
        FamilyMember familyMember = new FamilyMember();
        familyMember.setName(name);
        familyMember.setBorn(born);

        this.familyMembers.add(familyMember);

        return familyMember;
    }

    public List<FamilyMember> getFamilyMembers()
    {
        return this.familyMembers;
    }

    public boolean hasAddress()
    {
        return this.city != null || this.street != null || this.postalCode != null;
    }

    public boolean hasPhoneNumbers()
    {
        return this.mobilePhone != null || this.homePhone != null;
    }

    public boolean hasFamilyMembers()
    {
        return this.familyMembers.size() > 0;
    }
}