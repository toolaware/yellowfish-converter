package yellowfish.convert;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.StringWriter;

/**
 * Converts a People object to an XML string.
 */
public class PeopleXmlWriter
{
    public String toXml(People people) throws Exception
    {
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        StringWriter stringWriter = new StringWriter();

        XMLStreamWriter writer = outputFactory.createXMLStreamWriter(stringWriter);

        writer.writeStartDocument("UTF-8", "1.0");
            writer.writeStartElement("people");
                for (Person person : people.getPersons())
                    writePerson(person, writer);
            writer.writeEndElement();
        writer.writeEndDocument();

        writer.flush();
        stringWriter.flush();

        return stringWriter.toString();
    }

    private void writePerson(Person person, XMLStreamWriter writer) throws Exception
    {
        writer.writeStartElement("person");

            writeElement("firstname", person.getFirstName(), writer);
            writeElement("lastname", person.getLastName(), writer);

            if (person.hasAddress())
                writeAddress(person.getStreet(), person.getCity(), person.getPostalCode(), writer);

            if (person.hasPhoneNumbers())
                writePhoneNumbers(person.getMobilePhone(), person.getHomePhone(), writer);

            if (person.hasFamilyMembers())
                writeFamilyMembers(person, writer);

        writer.writeEndElement();
    }

    private void writeAddress(String street, String city, String postalCode, XMLStreamWriter writer) throws Exception
    {
        writer.writeStartElement("address");
            writeElement("street", street, writer);
            writeElement("city", city, writer);
            writeElement("postalcode", postalCode, writer);
        writer.writeEndElement();
    }

    private void writePhoneNumbers(String mobilePhone, String homePhone, XMLStreamWriter writer) throws Exception
    {
        writer.writeStartElement("phone");
            writeElement("mobile", mobilePhone, writer);
            writeElement("home", homePhone, writer);
        writer.writeEndElement();
    }

    private void writeFamilyMembers(Person person, XMLStreamWriter writer) throws Exception
    {
        for (FamilyMember familyMember : person.getFamilyMembers())
        {
            writer.writeStartElement("family");

                writeElement("name", familyMember.getName(), writer);
                writeElement("born", familyMember.getBorn(), writer);

                if (familyMember.hasAddress())
                    writeAddress(familyMember.getStreet(), familyMember.getCity(), familyMember.getPostalCode(), writer);

                if (familyMember.hasPhoneNumbers())
                    writePhoneNumbers(familyMember.getMobilePhone(), familyMember.getHomePhone(), writer);

            writer.writeEndElement();
        }
    }

    private void writeElement(String element, String value, XMLStreamWriter writer) throws Exception
    {
        if (value != null)
        {
            writer.writeStartElement(element);
                writer.writeCharacters(value);
            writer.writeEndElement();
        }
    }
}
