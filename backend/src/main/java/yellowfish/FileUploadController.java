package yellowfish;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import yellowfish.convert.People;
import yellowfish.convert.PeopleParser;
import yellowfish.convert.PeopleXmlWriter;

import java.io.InputStream;

@RestController
public class FileUploadController
{
    @Autowired
    public FileUploadController()
    {
    }

    @CrossOrigin
    @PostMapping("/convert")
    public ResponseEntity<String> convertFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws Exception
    {
        try
        {
            try (InputStream inputStream = file.getInputStream())
            {
                PeopleParser parser = new PeopleParser();
                People people = parser.parse(inputStream);

                PeopleXmlWriter xmlWriter = new PeopleXmlWriter();
                String xml = xmlWriter.toXml(people);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_XML);
                return new ResponseEntity<String>(xml, headers, HttpStatus.OK);
            }
        }
        catch (Exception e)
        {
            throw new Exception("Failed to parse and convert file", e);
        }
    }
}
