
(function() {
    'use strict';

    angular
        .module('Yellowfish')
        .constant('REST_URI', buildRestURI());

    function buildRestURI() {
        return location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + buildSubUrlFromPath(location.pathname.split('/')) + '/api';
    }

    function buildSubUrlFromPath(paths) {
        var path = [], startConcat = false;
        for (var i = paths.length - 1;i > -1;i--){
            if (paths[i] === 'app') // Since the base dir for this application is app, start there and concat the others
                startConcat = true;
            else if (startConcat)
                path.push(paths[i]);
        }

        return path.reverse().join('/');
    }
})();