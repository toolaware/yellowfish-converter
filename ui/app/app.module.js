(function() {
    'use strict';

    angular
        .module('Yellowfish', ['angular-file-input']);
})();