(function() {
    'use strict';

    angular
        .module('Yellowfish')
        .factory('Convert', Convert);

    Convert.$inject = ['$http', '$q', '$log', 'REST_URI'];

    function Convert($http, $q, $log, REST_URI) {
        var service = {
            uploadAndConvert: uploadAndConvert
        };

        return service;

        /////////////////

        function uploadAndConvert(file) {
            var deferred = $q.defer();

            var fileData = new FormData();
            fileData.append('file', file);

            var headers = {
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity()
            };

            $http.post('http://toolaware:8080' + '/convert', fileData, headers).then(
                function(response) {
                    deferred.resolve(response.data);
                },
                function(error) {
                    if (error.data)
                        $log.error('Failed to upload file for conversion, error: ' + error.data.message);

                    deferred.reject(error);
                }
            );

            return deferred.promise;
        }
    }
}());