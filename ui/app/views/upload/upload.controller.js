
(function() {
    'use strict';

    angular
        .module('Yellowfish')
        .controller('UploadController', UploadController);

    UploadController.$inject = ['Convert', '$q'];

    function UploadController(Convert, $q) {
        var vm = this;

        vm.submit = submit;

        //////////

        vm.error = false;
        vm.file = null;
        vm.result = null;

        //////////

        activate();

        function activate() {
        }

        function submit() {
            vm.error = false;
            vm.result = null;

            readOriginalText().then(
                function(text) {
                    Convert.uploadAndConvert(vm.file).then(
                        function(result) {
                            vm.result = {
                                source: {
                                    fileName: vm.file.name,
                                    data: text
                                },
                                target: formatXml(result)
                            };
                        },
                        function() {
                            vm.error = true;
                        }
                    );
                }
            );
        }

        function readOriginalText() {
            var deferred = $q.defer();

            var reader = new FileReader();

            reader.onload = function() {
                deferred.resolve(reader.result);
            };

            reader.readAsText(vm.file);

            return deferred.promise;
        }
    }
})();